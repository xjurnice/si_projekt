/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menza;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 *
 * @author Tom
 */
public class Menza {

    public Queue<Integer> fronta1 = new LinkedList<>();
    public Queue<Integer> fronta2 = new LinkedList<>();

    public int randInt(int min, int max) { //univerzalni metoda pro generovani cisla v intervalu

        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public int pocetStravniku(int hodina) { //na zaklade urcite hodiny se bude generovat nahodne cislo poctu stravniku
        int pocStravniku = 0;
        switch (hodina) {
            case 11:
                pocStravniku = this.randInt(200, 500);
                break;
            case 12:
                pocStravniku = this.randInt(300, 450);
                break;
            case 13:
                pocStravniku = this.randInt(150, 300);
                break;
            case 14:
                pocStravniku = this.randInt(50, 150);
                break;
            case 15:
                pocStravniku = this.randInt(30, 100);
                break;

        }
        return pocStravniku;
    }

    public int tranformace() { //tato metoda bude obsahovat transformaci stravnika ze stavu neobslouzeny do stavu obslouzeny
        int prohlizeniVitriny = this.randInt(1, 60); //nahodna doba prohlizeni vitriny
        int vezmePiti = this.randInt(1, 100); // nahodna hodnota pro urceni pravdepodobnosti 100 hodnot = 100%
        int casPiti = 0;
        if (vezmePiti >= 1 || vezmePiti <= 70) { //70% sance, ze si vezme piti
            casPiti = 10;
        }

        return prohlizeniVitriny + casPiti;
    }

    public static void main(String[] args) {
        Menza menzaX = new Menza();
        int hodina = menzaX.randInt(11, 15);
        System.out.println("Merena hodina je: " + hodina);
        int pocStravniku = menzaX.pocetStravniku(hodina);
        System.out.println("Pocet stravniku " + pocStravniku);

        int casJednoho = menzaX.tranformace();
        int sumaCasu = casJednoho;
        int maxCas = casJednoho;
        int minCas = casJednoho;
        for (int i = 1; i < pocStravniku; i++) {
            int casJ = menzaX.tranformace();
            sumaCasu += casJ;
            if (casJ < minCas) {
                minCas = casJ;
            }
            if (casJ > maxCas) {
                maxCas = casJ;
            }

        }
        System.out.println("Celkovy cas obslouzeni  " + sumaCasu / 60 + " minut");
        System.out.println("Minimalni cas obslouzeni  " + minCas  + " sekund");
        System.out.println("Maximalni cas obslouzeni  " + maxCas  + " sekund");
    }

}
